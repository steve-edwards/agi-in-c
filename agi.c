#define	SPEED
//
//	Filename:	agi.c
//
#define	Version		"024"
//
//	Edit date:	2024-01-23
//
//	Facility:	Asterisk
//
//	Abstract:	Define stuff useful for interfacing with AGI.
//
//	Environment:	Unix, Asterisk
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	20-Jun-2004	SLE	Create.
//	001	27-Jun-2004	SLE	Add stream_themed_file().
//	002	28-Jun-2004	SLE	Add poll() to read_agi_environment().
//	003	20-Sep-2004	SLE	Add get_themed_data().
//					Add themed_ask().
//	004	31-Mar-2005	SLE	Add get_variable().
//	005	21-Jul-2005	SLE	Add find_themed_file().
//					Work with WAV files.
//	006	12-Jul-2006	SLE	Add find_file().
//					Add stream_file().
//	007	2007-01-22	SLE	Quote file names in STREAM FILE.
//	008	2007-08-07	SLE	Add ask().
//	009	2009-02-20	SLE	Add set_variable().
//	010	2009-03-23	SLE	Rename exec_verbose() to
//					  agi_verbose().
//					Add agi_set_status_failure().
//					Add agi_set_status_success().
//	011	2010-09-29	SLE	Skip #comments from stdin.
//	012	2011-03-01	SLE	Change agi_command to 1024
//					  bytes in exec_agi().
//	013	2012-07-18	SLE	Add members through 1.8.
//	014	2017-09-09	SLE	Add agi_hangup().
//	015	2019-06-19	SLE	Remove get_themed_data().
//					Remove stream_themed_file().
//					Remove themed_ask().
//					Add agi_get_integer_variable().
//	016	2019-06-25	SLE	Change agi_command to 1024
//					  everywhere.
//	017	2019-08-06	SLE	Add variable arguments to
//					  agi_get_variable().
//					Add variable arguments to
//					  agi_get_integer_variable().
//	018	2019-12-27	SLE	Add agi_get_full_variable().
//					Add agi_exec().
//	019	2021-02-08	SLE	Add agi_stopmonitor().
//					Remove agi_syslog();
//					Remove exec_agi().
//					Rename dump_agi_environment()
//					  to agi_dump_environment().
//					Rename stream_file() to
//					  agi_stream_file().
//	020	2021-02-26	SLE	Remove all.h.
//					Remove find_file().
//					Remove find_themed_file().
//	021	2021-05-19	SLE	Change agi_command[] to 2048.
//	022	2023-03-28	SLE	Add agi_append_variable.
//	023	2023-10-13	SLE	Add agi_monitor().
//					Add agi_mixmonitor().
//					Add agi_stopmixmonitor().
//	024	2024-01-23	SLE	Add set_context().
//					Add set_extension().
//					Add set_priority().

////////////////////////////////////////|//////////////////////////////////////
// ANSI include files
////////////////////////////////////////|//////////////////////////////////////
#include	<errno.h>
#include	<stdarg.h>
#include	<stddef.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<unistd.h>

////////////////////////////////////////|//////////////////////////////////////
// Operating system include files
////////////////////////////////////////|//////////////////////////////////////
#include	"syslog.h"
#include	<sys/poll.h>
#include	<sys/stat.h>

////////////////////////////////////////|//////////////////////////////////////
// Local include files
////////////////////////////////////////|//////////////////////////////////////
#include	"agi.h"
//#include	"all.h"
#define	global		/**/

////////////////////////////////////////|//////////////////////////////////////
// Defines
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Macros
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Typedefs
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////
global	AGI_ENVIRONMENT			agi_environment;

////////////////////////////////////////|//////////////////////////////////////
// Global functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// agi_append_variable()
//
// This function appends to a variable.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_append_variable
	(
	  const char			*variable_pointer
	, char				*value_pointer
	)
	{
	auto	char			temp[1024];

// get the variable
	agi_get_variable(variable_pointer, temp);

// already have a value?
	if	(0 != *temp)
		{
		strcat(temp, value_pointer);
		value_pointer = temp;
		}

// set the variable
	agi_exec("set variable \"%s\" \"%s\""
		, variable_pointer
		, value_pointer
		);
	if	(AGI_SUCCESS != agi_environment.result)
		{
		syslog(LOG_DEBUG
			, "%s was not set to %s."
			, variable_pointer
			, value_pointer
			);
		return(EXIT_FAILURE);
		}
	return(EXIT_SUCCESS);
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_ask()
//
// This function plays a file and then waits for a digit string.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_ask
	(
	  char				*file_name_pointer
	, int				timeout
	, int				max_digits
	, char				*escape_digits_pointer
	, char				*digit_string_pointer
	)
	{
	auto	char			agi_command[2048];
//	auto	int			status;
	auto	char			temp_digit_string[256];
	auto	char			wait_for_digit_command[256];

// housekeeping
	memset(wait_for_digit_command
		, 0
		, sizeof(wait_for_digit_command)
		);
	if	(0 == digit_string_pointer)
		{
		digit_string_pointer = temp_digit_string;
		}

// play the file
	sprintf(agi_command, "stream file \"%s\" %s"
		, file_name_pointer
		, "#*0123456789"
		);
	agi_exec(agi_command);
	if	(0 != agi_environment.result)
		{
		*digit_string_pointer++ = agi_environment.result;
		--max_digits;
		}

// collect the digits
	sprintf(wait_for_digit_command
		, "wait for digit %d"
		, timeout
		);
	for	(;;)
		{
		if	(0 >= max_digits)
			{
			break;
			}
		agi_exec(wait_for_digit_command);
		if	(0 == agi_environment.result)
			{
			break;
			}
		if	(0 != strchr(escape_digits_pointer
				, agi_environment.result))
			{
			break;
			}
		*digit_string_pointer++ = agi_environment.result;
		--max_digits;
		}
	*digit_string_pointer++ = 0;

// return the escape digit
	return(agi_environment.result);

	}

////////////////////////////////////////|//////////////////////////////////////
// agi_dump_environment()
//
// This function dumps the contents of the AGI environment to syslog.
////////////////////////////////////////|//////////////////////////////////////
global	void				agi_dump_environment
	(
	  AGI_ENVIRONMENT		*agi_environment_pointer
	)
	{
//	auto	int			status;

// see if we weren't passed a pointer to an environment
	if	(0 == agi_environment_pointer)
		{
		agi_environment_pointer = &agi_environment;
		}

// dump the members
	syslog(LOG_DEBUG, "Dumping AGI environment");
	syslog(LOG_DEBUG, "        code = \"%d\""		, agi_environment_pointer->code);
	syslog(LOG_DEBUG, "        endpos = \"%d\""		, agi_environment_pointer->endpos);
	syslog(LOG_DEBUG, "        result = \"%d\""		, agi_environment_pointer->result);
	syslog(LOG_DEBUG, "        agi_accountcode = \"%s\""	, agi_environment_pointer->agi_accountcode);
	syslog(LOG_DEBUG, "        agi_arg_1 = \"%s\""		, agi_environment_pointer->agi_arg_1);
	syslog(LOG_DEBUG, "        agi_arg_2 = \"%s\""		, agi_environment_pointer->agi_arg_2);
	syslog(LOG_DEBUG, "        agi_arg_3 = \"%s\""		, agi_environment_pointer->agi_arg_3);
	syslog(LOG_DEBUG, "        agi_arg_4 = \"%s\""		, agi_environment_pointer->agi_arg_4);
	syslog(LOG_DEBUG, "        agi_arg_5 = \"%s\""		, agi_environment_pointer->agi_arg_5);
	syslog(LOG_DEBUG, "        agi_arg_6 = \"%s\""		, agi_environment_pointer->agi_arg_6);
	syslog(LOG_DEBUG, "        agi_arg_7 = \"%s\""		, agi_environment_pointer->agi_arg_7);
	syslog(LOG_DEBUG, "        agi_arg_8 = \"%s\""		, agi_environment_pointer->agi_arg_8);
	syslog(LOG_DEBUG, "        agi_arg_9 = \"%s\""		, agi_environment_pointer->agi_arg_9);
	syslog(LOG_DEBUG, "        agi_callerid = \"%s\""	, agi_environment_pointer->agi_callerid);
	syslog(LOG_DEBUG, "        agi_calleridname = \"%s\""	, agi_environment_pointer->agi_calleridname);
	syslog(LOG_DEBUG, "        agi_callingani2 = \"%s\""	, agi_environment_pointer->agi_callingani2);
	syslog(LOG_DEBUG, "        agi_callingpres = \"%s\""	, agi_environment_pointer->agi_callingpres);
	syslog(LOG_DEBUG, "        agi_callingtns = \"%s\""	, agi_environment_pointer->agi_callingtns);
	syslog(LOG_DEBUG, "        agi_callington = \"%s\""	, agi_environment_pointer->agi_callington);
	syslog(LOG_DEBUG, "        agi_channel = \"%s\""	, agi_environment_pointer->agi_channel);
	syslog(LOG_DEBUG, "        agi_context = \"%s\""	, agi_environment_pointer->agi_context);
	syslog(LOG_DEBUG, "        agi_dnid = \"%s\""		, agi_environment_pointer->agi_dnid);
	syslog(LOG_DEBUG, "        agi_enhanced = \"%s\""	, agi_environment_pointer->agi_enhanced);
	syslog(LOG_DEBUG, "        agi_extension = \"%s\""	, agi_environment_pointer->agi_extension);
	syslog(LOG_DEBUG, "        agi_language = \"%s\""	, agi_environment_pointer->agi_language);
	syslog(LOG_DEBUG, "        agi_priority = \"%s\""	, agi_environment_pointer->agi_priority);
	syslog(LOG_DEBUG, "        agi_rdnis = \"%s\""		, agi_environment_pointer->agi_rdnis);
	syslog(LOG_DEBUG, "        agi_request = \"%s\""	, agi_environment_pointer->agi_request);
	syslog(LOG_DEBUG, "        agi_threadid = \"%s\""	, agi_environment_pointer->agi_threadid);
	syslog(LOG_DEBUG, "        agi_type = \"%s\""		, agi_environment_pointer->agi_type);
	syslog(LOG_DEBUG, "        agi_uniqueid = \"%s\""	, agi_environment_pointer->agi_uniqueid);
	syslog(LOG_DEBUG, "        agi_version = \"%s\""	, agi_environment_pointer->agi_version);
	syslog(LOG_DEBUG, "        response = \"%s\""		, agi_environment_pointer->response);
	syslog(LOG_DEBUG, "        variable = \"%s\""		, agi_environment_pointer->variable);
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_exec()
//
// This function executes an AGI command and parses the response from
// Asterisk.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_exec
	(
	  char				*agi_command_pointer
	, ...				// variable argument list
	)
	{
	auto	char			agi_command[20480];
	auto	va_list			argument_pointer;
	auto	char			*pointer;

// say what's going on
	syslog(LOG_DEBUG
		, "agi_exec(\"%s\")"
		, agi_command_pointer
		);

// process our variable argument list
	memset(agi_command, 0, sizeof(agi_command));
	va_start(argument_pointer, agi_command_pointer);
	vsnprintf(agi_command
		, sizeof(agi_command)
		, agi_command_pointer
		, argument_pointer
		);
	va_end(argument_pointer);

// append a \n if needed
	pointer = agi_command + strlen(agi_command);
	--pointer;
	if	('\n' != *pointer)
		{
		++pointer;
		*pointer++ = '\n';
		*pointer = 0;
		}

// say exactly what's going on
	syslog(LOG_DEBUG
		, "agi_exec(\"%s\")"
		, agi_command
		);

// issue the command
	fputs(agi_command, stdout);
	fflush(stdout);

// reset the output values
	agi_environment.code
		= agi_environment.endpos
		= agi_environment.result
			= 0;
	memset(agi_environment.response, 0, sizeof(agi_environment.response));
	memset(agi_environment.variable, 0, sizeof(agi_environment.variable));

// read the response
	fgets(agi_environment.response
		, sizeof(agi_environment.response)
		, stdin
		);
	syslog(LOG_DEBUG
		, "\"%s\""
		, agi_environment.response
		);

// find the code
	agi_environment.code = atoi(agi_environment.response);

// find the result
	pointer = strstr(agi_environment.response, "result=");
	if	(0 != pointer)
		{
		agi_environment.result = atoi(pointer + 7);
		}

// find the endpos
	pointer = strstr(agi_environment.response, "endpos=");
	if	(0 != pointer)
		{
		agi_environment.endpos = atoi(pointer + 7);
		}

// extract the variable
	pointer = strchr(agi_environment.response, '(');
	if	(0 != pointer)
		{
		strcpy(agi_environment.variable, pointer + 1);
		pointer = strrchr(agi_environment.variable, ')');
		*pointer = 0;
		}

// log the response
//	syslog(LOG_DEBUG
//		, "response = \"%s\", code = %d, result = %d, endpos = %d"
//		, agi_environment.response
//		, agi_environment.code
//		, agi_environment.result
//		, agi_environment.endpos
//		);

// fudge to cause an invalid code to signal a failure
	if	(0 == agi_environment.code)
		{
		agi_environment.result = -1;
		syslog(LOG_DEBUG
			, "result fudged to -1"
			);
		}

// return the result
	return(agi_environment.result);

	}

////////////////////////////////////////|//////////////////////////////////////
// agi_get_full_variable()
//
// This function gets a variable.
//
// This function understands complex variable names and builtin
// variables, unlike agi_get_variable().
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_get_full_variable
	(
	  const char			*variable_pointer
	, char				*value_pointer
	)
	{

// say what's going on
	syslog(LOG_DEBUG
		, "agi_get_full_variable(\"%s\", \"%s\")"
		, variable_pointer
		, value_pointer
		);

// get the variable
	agi_exec("get full variable \"%s\"", variable_pointer);
	if	(AGI_SUCCESS != agi_environment.result)
		{
		syslog(LOG_DEBUG
			, "%s is not in the environment."
			, variable_pointer
			);
		return(AGI_FAILURE);
		}
	strcpy(value_pointer, agi_environment.variable);
	return(AGI_SUCCESS);
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_get_integer_variable()
//
// This function gets a variable and returns it as an integer.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_get_integer_variable
	(
	  const char			*variable_pointer
	, ...				// variable argument list
	)
	{
	auto	char			agi_command[2048];
	auto	va_list			argument_pointer;
	auto	char			temp[256];

// process our variable argument list
	memset(agi_command, 0, sizeof(agi_command));
	va_start(argument_pointer, variable_pointer);
	vsnprintf(agi_command
		, sizeof(agi_command)
		, variable_pointer
		, argument_pointer
		);
	va_end(argument_pointer);

// get the variable
	memset(temp, 0, sizeof(temp));
	agi_get_variable(agi_command, temp);
	return(atoi(temp));
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_get_variable()
//
// This function gets a variable.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_get_variable
	(
	  const char			*variable_pointer
	, char				*value_pointer
	, ...				// variable argument list
	)
	{
	auto	char			agi_command[2048];
	auto	va_list			argument_pointer;

// process our variable argument list
	memset(agi_command, 0, sizeof(agi_command));
	va_start(argument_pointer, value_pointer);
	vsnprintf(agi_command
		, sizeof(agi_command)
		, variable_pointer
		, argument_pointer
		);
	va_end(argument_pointer);

// get the variable
	agi_exec("get variable \"%s\"", agi_command);
	if	(AGI_SUCCESS != agi_environment.result)
		{
		syslog(LOG_DEBUG
			, "%s is not in the environment."
			, agi_command
			);
		*value_pointer = 0;
		return(EXIT_FAILURE);
		}
	strcpy(value_pointer, agi_environment.variable);
	return(EXIT_SUCCESS);
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_hangup()
//
// This function hangs up a channel.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_hangup
	(
	  void
	)
	{

// hang up the channel
	agi_exec("hangup");
	return(EXIT_SUCCESS);

	}

////////////////////////////////////////|//////////////////////////////////////
// agi_mixmonitor()
//
// This function starts mixmonitor.
// MixMonitor(filename.extension[,options[,command]])
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_mixmonitor
	(
	  const char			*filename_pointer
	, const char			*options_pointer
	, const char			*command_pointer
	)
	{

// start mixmonitoring
	agi_exec("exec mixmonitor \"%s\",\"%s\",\"%s\""
		, filename_pointer
		, options_pointer
		, command_pointer
		);
	return(EXIT_SUCCESS);

	}

////////////////////////////////////////|//////////////////////////////////////
// agi_monitor()
//
// This function starts monitor.
// Monitor([file_format[:urlbase]][,fname_base[,options]])
////////////////////////////////////////|//////////////////////////////////////
global	void				agi_monitor
	(
	  void
	)
	{
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_read_environment()
//
// This function reads the AGI environment.
////////////////////////////////////////|//////////////////////////////////////
global	void				agi_read_environment
	(
	  void
	)
	{
	auto	char			line[256];
//	auto	int			status;
// the order of the member_list array must track any changes to the
// AGI_ENVIRONMENT structure.
	static	const char		member_list[]
		= "\
XXXXXXXXXXXX|\
accountcode |\
arg_1       |\
arg_2       |\
arg_3       |\
arg_4       |\
arg_5       |\
arg_6       |\
arg_7       |\
arg_8       |\
arg_9       |\
callerid    |\
calleridname|\
callingani2 |\
callingpres |\
callingtns  |\
callington  |\
channel     |\
context     |\
dnid        |\
enhanced    |\
extension   |\
language    |\
priority    |\
rdnis       |\
request     |\
threadid    |\
type        |\
uniqueid    |\
version     |\
XXXXXXXXXXXX";
// the order of the member_offset array must track any changes to the
// AGI_ENVIRONMENT structure.
	static	const int		member_offsets[]
		= {
			  0
			, offsetof(AGI_ENVIRONMENT, agi_accountcode)
			, offsetof(AGI_ENVIRONMENT, agi_arg_1)
			, offsetof(AGI_ENVIRONMENT, agi_arg_2)
			, offsetof(AGI_ENVIRONMENT, agi_arg_3)
			, offsetof(AGI_ENVIRONMENT, agi_arg_4)
			, offsetof(AGI_ENVIRONMENT, agi_arg_5)
			, offsetof(AGI_ENVIRONMENT, agi_arg_6)
			, offsetof(AGI_ENVIRONMENT, agi_arg_7)
			, offsetof(AGI_ENVIRONMENT, agi_arg_8)
			, offsetof(AGI_ENVIRONMENT, agi_arg_9)
			, offsetof(AGI_ENVIRONMENT, agi_callerid)
			, offsetof(AGI_ENVIRONMENT, agi_calleridname)
			, offsetof(AGI_ENVIRONMENT, agi_callingani2)
			, offsetof(AGI_ENVIRONMENT, agi_callingpres)
			, offsetof(AGI_ENVIRONMENT, agi_callingtns)
			, offsetof(AGI_ENVIRONMENT, agi_callington)
			, offsetof(AGI_ENVIRONMENT, agi_channel)
			, offsetof(AGI_ENVIRONMENT, agi_context)
			, offsetof(AGI_ENVIRONMENT, agi_dnid)
			, offsetof(AGI_ENVIRONMENT, agi_enhanced)
			, offsetof(AGI_ENVIRONMENT, agi_extension)
			, offsetof(AGI_ENVIRONMENT, agi_language)
			, offsetof(AGI_ENVIRONMENT, agi_priority)
			, offsetof(AGI_ENVIRONMENT, agi_rdnis)
			, offsetof(AGI_ENVIRONMENT, agi_request)
			, offsetof(AGI_ENVIRONMENT, agi_threadid)
			, offsetof(AGI_ENVIRONMENT, agi_type)
			, offsetof(AGI_ENVIRONMENT, agi_uniqueid)
			, offsetof(AGI_ENVIRONMENT, agi_version)
		};
//	auto	struct pollfd		ufds
//		= {STDIN_FILENO, -1, 0};

#ifndef	SPEED
// initialize the environment structure
	memset(&agi_environment, 0, sizeof(agi_environment));

#endif

// check to see if stdin has anything in it
//	status = poll(&ufds, 1, 0);
//	syslog(LOG_DEBUG
//		, "ufds.revents = %d"
//		, ufds.revents
//		);

// Bits:
//	Symbol		Value		Description
//	------		-----		-----------
//	POLLIN		0x001		There is data to read.
//	POLLPRI		0x002		There is urgent data to read.
//	POLLOUT		0x004		Writing now will not block.
//	POLLERR		0x008		Error condition.
//	POLLHUP		0x010		Hung up.
//	POLLNVAL	0x020		Invalid polling request.
//	POLLRDNORM	0x040		Normal data may be read.
//	POLLRDBAND	0x080		Priority data may be read.
//	POLLWRNORM	0x100		Writing now will not block.
//	POLLWRBAND	0x200		Priority data may be written.
//	POLLMSG		0x400
//
// Observed values:
//	Dec	Hex	Bin		Description
//	---	---	---		-----------
//	 65	0x041	001000001	Invoked from Asterisk with stdin
//					POLLIN + POLLRDNORM
//	 81	0x051	001010001	Invoked from tty with echo feeding
//					  stdin
//					POLLIN + POLLRDBAND
//	260	0x104	100000100	Invoked from tty without stdin
//					POLLOUT + POLLWRNORM
//	325	0x145	101000101	Invoked from tty with stdin
//					  redirected to a file
//					POLLIN + POLLOUT + POLLRDNORM
//					  + POLLWRNORM

// return if there is no data to read
//	if	(0 == (POLLIN & ufds.revents))
//		{
//		return;
// too flakey on dt
//		}

// read the environment
	memset(line, 0, sizeof(line));
	while	(0 != fgets(line, sizeof(line), stdin))
		{
		auto	int		len;
		auto	int		member_index;
		auto	char		*member_pointer;
		auto	char		*value_pointer;
		len = strlen(line);
		if	(len <= 1)
			{
			break;
			}
		--len;
		*(line + len) = 0;

// skip comments
		if	('#' == *line)
			{
			continue;
			}

// find the value
		value_pointer = strstr(line, ": ");
		if	(0 == value_pointer)
			{
			syslog(LOG_WARNING
				, "The value separator is missing from \"%s\"."
				, line
				);
			continue;
			}
		*value_pointer++ = 0;
		*value_pointer++ = 0;

// find the member index
//		*(line + ) = 0;
//syslog(LOG_ERR, "\"%s\" \"%s\"", member_list, line);
		member_index = strstr(member_list, line + 4) - member_list;
		if	(0 > member_index)
			{
			syslog(LOG_WARNING
				, "The member index for \"%s\" was not found."
				, line + 4
				);
			continue;
			}
		member_index /= 13;

// save the value
		member_pointer = (char *)&agi_environment;
		member_pointer += member_offsets[member_index];
		strcpy(member_pointer, value_pointer);
		}
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_set_context()
//
// This function sets the context.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_set_context
	(
	  char				*value_pointer
	)
	{

// set the context
	agi_exec("set context \"%s\""
		, value_pointer
		);
	if	(AGI_SUCCESS != agi_environment.result)
		{
		syslog(LOG_DEBUG
			, "CONTEXT was not set to %s."
			, value_pointer
			);
		return(EXIT_FAILURE);
		}
	return(EXIT_SUCCESS);
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_set_extension()
//
// This function sets the extension.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_set_extension
	(
	  char				*value_pointer
	)
	{

// set the extension
	agi_exec("set extension \"%s\""
		, value_pointer
		);
	if	(AGI_SUCCESS != agi_environment.result)
		{
		syslog(LOG_DEBUG
			, "EXTENSION was not set to %s."
			, value_pointer
			);
		return(EXIT_FAILURE);
		}
	return(EXIT_SUCCESS);
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_set_priority()
//
// This function sets the priority.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_set_priority
	(
	  char				*value_pointer
	)
	{

// set the priority
	agi_exec("set priority \"%s\""
		, value_pointer
		);
	if	(AGI_SUCCESS != agi_environment.result)
		{
		syslog(LOG_DEBUG
			, "PRIORITY was not set to %s."
			, value_pointer
			);
		return(EXIT_FAILURE);
		}
	return(EXIT_SUCCESS);
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_set_integer_variable()
//
// This function sets a variable from an integer.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_set_integer_variable
	(
	  const char			*variable_pointer
	, int				value
	)
	{
	auto	char			temp[16];

// convert the int to a string
	sprintf(temp
		, "%d"
		, value
		);

// set the variable
	return(agi_set_variable(variable_pointer, temp));

	}

////////////////////////////////////////|//////////////////////////////////////
// agi_set_status_failure()
//
// This function sets a variable.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_set_status_failure
	(
	  void
	)
	{
	return(agi_set_variable("STATUS", "FAILURE"));
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_set_status_success()
//
// This function sets a variable.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_set_status_success
	(
	  void
	)
	{
	return(agi_set_variable("STATUS", "SUCCESS"));
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_set_variable()
//
// This function sets a variable.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_set_variable
	(
	  const char			*variable_pointer
	, char				*value_pointer
	)
	{

// set the variable
	agi_exec("set variable \"%s\" \"%s\""
		, variable_pointer
		, value_pointer
		);
	if	(AGI_SUCCESS != agi_environment.result)
		{
		syslog(LOG_DEBUG
			, "%s was not set to %s."
			, variable_pointer
			, value_pointer
			);
		return(EXIT_FAILURE);
		}
	return(EXIT_SUCCESS);
	}

////////////////////////////////////////|//////////////////////////////////////
// agi_stopmixmonitor()
//
// This function stops mixmonitoring.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_stopmixmonitor
	(
	  char				*mixmonitorid
	)
	{

// stop monitoring
	if	(0 == mixmonitorid)
		{
		agi_exec("exec stopmixmonitor");
		}
	else
		{
		agi_exec("exec stopmixmonitor %s"
			 , mixmonitorid
			);
		}

	return(EXIT_SUCCESS);

	}

////////////////////////////////////////|//////////////////////////////////////
// agi_stopmonitor()
//
// This function stops monitoring.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_stopmonitor
	(
	  void
	)
	{

// stop monitoring
	agi_exec("exec stopmonitor");
	return(EXIT_SUCCESS);

	}

////////////////////////////////////////|//////////////////////////////////////
// agi_stream_file()
//
// This function streams a file.
////////////////////////////////////////|//////////////////////////////////////
global	int				agi_stream_file
	(
	  char				*file_name_pointer
	, char				*escape_digits_pointer
	)
	{
	auto	char			agi_command[2048];
//	auto	int			status;

	syslog(LOG_DEBUG
		, "stream_file()"
		);

// build the agi command
	sprintf(agi_command, "stream file \"%s\" %s"
		, file_name_pointer
		, escape_digits_pointer
		);

// execute the agi command and return
	return(agi_exec(agi_command));

	}

////////////////////////////////////////|//////////////////////////////////////
// agi_verbose()
//
// This function executes the AGI VERBOSE command.
////////////////////////////////////////|//////////////////////////////////////
global	void				agi_verbose
	(
	  char				*agi_command_pointer
	, ...				// variable argument list
	)
	{
	auto	char			agi_command[2048];
	auto	va_list			argument_pointer;
	auto	char			verbose_text[2048];

// prefix and suffix the verbose text
	sprintf(verbose_text, "verbose \"%s\" 0", agi_command_pointer);

// process our variable argument list
	memset(agi_command, 0, sizeof(agi_command));
	va_start(argument_pointer, agi_command_pointer);
	vsnprintf(agi_command
		, sizeof(agi_command)
		, verbose_text
		, argument_pointer
		);
	va_end(argument_pointer);

// hand it off to agi_exec()
	agi_exec(agi_command);

	}

// (end of agi.c)
