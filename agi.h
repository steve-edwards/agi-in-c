//
//	Filename:	agi.h
//
//	Version:	"026"
//
//	Edit date:	2024-01-23
//
//	Facility:	Asterisk
//
//	Abstract:	Define stuff useful for interfacing with AGI.
//
//	Environment:	Unix, Asterisk
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	20-Jun-2004	SLE	Create.
//	001	27-Jun-2004	SLE	Add stream_themed_file().
//	002	10-Dec-2004	SLE	Add calleridname to environment.
//	003	29-Mar-2005	SLE	Add callingani2 to environment.
//					Add callingpres to environment.
//					Add callingtns to environment.
//					Add callington to environment.
//	004	31-Mar-2005	SLE	Add get_variable().
//	005	07-Aug-2005	SLE	Add agi_mode.
//	006	12-Jul-2006	SLE	Add stream_file().
//	007	2007-07-30	SLE	Add POUND_EXIT.
//	008	2007-08-07	SLE	Add STAR_EXIT.
//					Add ask().
//	009	2009-02-20	SLE	Add set_variable().
//	010	2009-03-23	SLE	Rename exec_verbose() to
//					  agi_verbose().
//					Add agi_set_status_failure().
//					Add agi_set_status_success().
//	011	2011-02-25	SLE	Change the size of
//					  AGI_ENVIRONMENT.response to
//					  1024.
//					Change the size of
//					  AGI_ENVIRONMENT.variable to
//					  1024.
//	012	2012-07-18	SLE	Add members through 1.8.
//	013	2017-09-09	SLE	Add agi_hangup().
//	014	2018-07-03	SLE	Add agi_set_integer_variable().
//	015	2019-06-19	SLE	Remove get_themed_data().
//					Remove stream_themed_file().
//					Remove themed_ask().
//					Add agi_get_integer_variable().
//	016	2019-08-06	SLE	Add variable arguments to
//					  agi_get_variable().
//					Add variable arguments to
//					  agi_get_integer_variable().
//	017	2019-12-27	SLE	Add agi_get_full_variable().
//					Add agi_exec().
//	018	2021-01-31	SLE	Change agi_environment to
//					  global.
//	019	2021-02-08	SLE	Add agi_stopmonitor().
//					Remove agi_syslog();
//					Remove exec_agi().
//					Rename dump_agi_environment()
//					  to agi_dump_environment().
//					Rename stream_file() to
//					  agi_stream_file().
//	020	2021-02-26	SLE	Remove all.h.
//					Remove AGI_FATAL().
//					Remove POUND_EXIT.
//					Remove STAR_EXIT.
//	021	2021-05-19	SLE	Change response to [2048].
//	022	2022-02-10	SLE	Declare agi_environment weak.
//	023	2023-01-08	SLE	Change variable to [2048].
//	024	2023-03-28	SLE	Add agi_append_variable.
//	025	2023-10-09	SLE	Add agi_monitor().
//					Add agi_mixmonitor().
//					Add agi_stopmixmonitor().
//	026	2024-01-23	SLE	Add set_context().
//					Add set_extension().
//					Add set_priority().

////////////////////////////////////////|//////////////////////////////////////
// ANSI include files
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Operating system include files
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Local include files
////////////////////////////////////////|//////////////////////////////////////
//#include	"all.h"
#define	global		/**/

////////////////////////////////////////|//////////////////////////////////////
// Defines
////////////////////////////////////////|//////////////////////////////////////
#define	AGI_FAILURE			0
#define	AGI_SUCCESS			1

////////////////////////////////////////|//////////////////////////////////////
// Macros
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Typedefs
////////////////////////////////////////|//////////////////////////////////////
typedef	struct
	{
// ints
	int				code;
	int				endpos;
	int				result;
// strings
	char				agi_accountcode[256];
	char				agi_arg_1[256];
	char				agi_arg_2[256];
	char				agi_arg_3[256];
	char				agi_arg_4[256];
	char				agi_arg_5[256];
	char				agi_arg_6[256];
	char				agi_arg_7[256];
	char				agi_arg_8[256];
	char				agi_arg_9[256];
	char				agi_callerid[256];
	char				agi_calleridname[256];
	char				agi_callingani2[256];
	char				agi_callingpres[256];
	char				agi_callingtns[256];
	char				agi_callington[256];
	char				agi_channel[256];
	char				agi_context[256];
	char				agi_dnid[256];
	char				agi_enhanced[4];
	char				agi_extension[256];
	char				agi_language[256];
	char				agi_priority[256];
	char				agi_rdnis[256];
	char				agi_request[256];
	char				agi_threadid[256];
	char				agi_type[256];
	char				agi_uniqueid[256];
	char				agi_version[256];
	char				response[2048];
	char				variable[2048];
	}				AGI_ENVIRONMENT;

////////////////////////////////////////|//////////////////////////////////////
// Global constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////
#pragma	weak				agi_environment
global	AGI_ENVIRONMENT			agi_environment;

////////////////////////////////////////|//////////////////////////////////////
// Global functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External functions
////////////////////////////////////////|//////////////////////////////////////
extern	int				agi_append_variable
	(
	  const char			*variable_pointer
	, char				*value_pointer
	);
extern	int				agi_ask
	(
	  char				*file_name_pointer
	, int				timeout
	, int				max_digits
	, char				*escape_digits_pointer
	, char				*digit_string_pointer
	);
extern	void				agi_dump_environment
	(
	  AGI_ENVIRONMENT		*agi_environment_pointer
	);
extern	int				agi_exec
	(
	  char				*agi_command
	, ...				// variable argument list
	);
extern	int				agi_get_full_variable
	(
	  const char			*variable_pointer
	, char				*value_pointer
	);
extern	int				agi_get_integer_variable
	(
	  const char			*variable_pointer
	, ...				// variable argument list
	);
extern	int				agi_get_variable
	(
	  const char			*variable_pointer
	, char				*value_pointer
	, ...				// variable argument list
	);
extern	int				agi_hangup
	(
	  void
	);
extern	int				agi_mixmonitor
	(
	  const char			*filename_pointer
	, const char			*options_pointer
	, const char			*command_pointer
	);
extern	void				agi_monitor
	(
	  void
	);
extern	void				agi_read_environment
	(
	  void
	);
extern	int				agi_set_context
	(
	  char				*value_pointer
	);
extern	int				agi_set_extension
	(
	  char				*value_pointer
	);
extern	int				agi_set_priority
	(
	  char				*value_pointer
	);
extern	int				agi_set_integer_variable
	(
	  const char			*variable_pointer
	, int				value
	);
extern	int				agi_set_status_failure
	(
	  void
	);
extern	int				agi_set_status_success
	(
	  void
	);
extern	int				agi_set_variable
	(
	  const char			*variable_pointer
	, char				*value_pointer
	);
extern	int				agi_stopmixmonitor
	(
	  char				*mixmonitorid
	);
extern	int				agi_stopmonitor
	(
	  void
	);
extern	int				agi_stream_file
	(
	  char				*file_name_pointer
	, char				*escape_digits_pointer
	);
extern	void				agi_verbose
	(
	  char				*agi_command
	, ...				// variable argument list
	);

////////////////////////////////////////|//////////////////////////////////////
// Static constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static functions
////////////////////////////////////////|//////////////////////////////////////

// (end of agi.h)
