# agi-in-c

An Asterisk AGI library in C

This is my AGI library for C. It has never been published or used by
anyone other than myself, so expect some rough edges.

Included is an example program (example.c) incorporating the elements
of what I consider to be a well written AGI:

1. Trapping SIGHUP
1. Using syslog()
1. Using getopt_long()
1. Initializing STATUS to FAILURE, setting to SUCCESS on completion

After compiling and installing the example AGI in your AST_AGI_DIR
directory, you can execute it with a dialplan similar to:

```
; example AGI
	same = n,			set(TEST=12345)
	same = n,			agi(example,--city=san-diego,--verbose)
	same = n,			hangup()
```
If you make any changes to the source, please forward them to me.

Likewise, any feedback -- good or bad will be appreciated.

Steve Edwards  
Newline  
sedwards@sedwards.com

### (end of README.md)
