//
//	Filename:	agi-mysql.c
//
#define	Version		"004"
//
//	Edit date:	2023-04-06
//
//	Facility:	Asterisk
//
//	Abstract:	Define stuff useful for interfacing MySQL with
//			AGI.
//
//	Environment:	Unix/MySQL
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	2009-03-23	SLE	Create.
//	001	2021-01-15	SLE	Add syslog_prefix.
//	002	2021-03-06	SLE	Rename database_* to local_*.
//	003	2022-02-10	SLE	Declare syslog_prefix weak.
//	004	2023-04-06	SLE	Try another way to do weak.
//					Change syslog priority to
//					  LOG_ERR in exec_sql().

////////////////////////////////////////|//////////////////////////////////////
// ANSI include files
////////////////////////////////////////|//////////////////////////////////////
#include	<ctype.h>
#include	<errno.h>
#include	<stdarg.h>
#include	<stddef.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

////////////////////////////////////////|//////////////////////////////////////
// Operating system include files
////////////////////////////////////////|//////////////////////////////////////
#include	<syslog.h>

////////////////////////////////////////|//////////////////////////////////////
// Local include files
////////////////////////////////////////|//////////////////////////////////////
#include	"agi.h"
#include	"all.h"
#include	"getopt.h"
#include	"mysql.h"

////////////////////////////////////////|//////////////////////////////////////
// Defines
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Macros
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Typedefs
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////
global	char				syslog_prefix[256] __attribute__((weak));

////////////////////////////////////////|//////////////////////////////////////
// Global functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|///////////////////////////////|//////
// agi_connect_database()
//
// This function connects to the database specified in the channel
// variables.
////////////////////////////////////////|///////////////////////////////|//////
global	int				agi_connect_to_database
	(
	  MYSQL				*mysql_pointer
	)
	{
	auto	char			local_database[256];
	auto	char			local_password[256];
	auto	char			local_server[256];
	auto	char			local_username[256];

// get the database database
	memset(local_database, 0, sizeof(local_database));
	agi_get_variable("LOCAL-DATABASE", local_database);

// get the database password
	memset(local_password, 0, sizeof(local_password));
	agi_get_variable("LOCAL-PASSWORD", local_password);

// get the database server
	memset(local_server, 0, sizeof(local_server));
	agi_get_variable("LOCAL-SERVER", local_server);
	if	(0 == *local_server)
		{
		agi_verbose("LOCAL-SERVER is undefined.");
		syslog(LOG_ERR
			, "LOCAL-SERVER is undefined."
			);
//		agi_hangup();
		return(EXIT_FAILURE);
		}

// get the database username
	memset(local_username, 0, sizeof(local_username));
	agi_get_variable("LOCAL-USERNAME", local_username);

// connect to the database
	mysql_init(mysql_pointer);
	if	(!mysql_real_connect(
			  mysql_pointer	// context
			, local_server	// host
			, local_username
					// username
			, local_password
					// password
			, local_database
					// database
			, 0		// port
			, 0		// unix_socket
			, 0		// client_flag
			))
		{
		syslog(LOG_ERR
			, "%m, MySQL_error = \"%s\""
			, mysql_error(mysql_pointer)
			);
		agi_verbose(
			  "Failed to connect to the database"
				", MySQL_error = \"%s\""
			, mysql_error(mysql_pointer)
			);
		return(EXIT_FAILURE);
		}

// return function status
	return(EXIT_SUCCESS);

	}

////////////////////////////////////////|///////////////////////////////|//////
// exec_sql()
//
// This function executes an SQL statement
////////////////////////////////////////|///////////////////////////////|//////
global	int				exec_sql
	(
	  MYSQL				*mysql_pointer
	, MYSQL_RES			**result_handle
	, const char			*sql_statement_pointer
	, ...				// variable argument list
	)
	{
	auto	va_list			argument_pointer;
	auto	char			*pointer1;
	auto	char			*pointer2;
	auto	char			sqlcmd[4096];

// process our variable argument list
	memset(sqlcmd, 0, sizeof(sqlcmd));
	va_start(argument_pointer, sql_statement_pointer);
	vsnprintf(sqlcmd
		, sizeof(sqlcmd)
		, sql_statement_pointer
		, argument_pointer
		);
	va_end(argument_pointer);

// clean up the statement
	pointer1
		= pointer2
		= sqlcmd;
	while	(0 != *pointer1)
		{
		if	(0 == iscntrl(*pointer1))
			{
			*pointer2++ = *pointer1;
			}
		else
			{
			*pointer2++ = ' ';
			}
		++pointer1;
		}
	*pointer2 = 0;

// log it
//	syslog(LOG_DEBUG
//		, "exec_sql(\"%s\")"
//		, sqlcmd
//		);

// process our variable argument list
	memset(sqlcmd, 0, sizeof(sqlcmd));
	va_start(argument_pointer, sql_statement_pointer);
	vsnprintf(sqlcmd
		, sizeof(sqlcmd)
		, sql_statement_pointer
		, argument_pointer
		);
	va_end(argument_pointer);

// log the statement
	syslog(LOG_ERR
		, "%s %s"
		, syslog_prefix
		, sqlcmd
		);

// execute the statement
	if	(0 != mysql_query(mysql_pointer, sqlcmd))
		{
		syslog(LOG_ERR
			, "MySQL_error = \"%s\""
			, mysql_error(mysql_pointer)
			);
		syslog(LOG_ERR
			, "%s"
			, sqlcmd
			);
		return(EXIT_FAILURE);
		}

// return the result if requested and this was a select
	if	(0 != result_handle)
		{
		if	(0 == (*result_handle
				= mysql_store_result(mysql_pointer)))
			{
			syslog(LOG_ERR
				, "%s"
				, sqlcmd
				);
			syslog(LOG_ERR
				, "MySQL_error = \"%s\""
				, mysql_error(mysql_pointer)
				);
			return(EXIT_FAILURE);
			}
		}
	return(EXIT_SUCCESS);
	}

// (end of exec-sql.c)
