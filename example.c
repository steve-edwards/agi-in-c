//
//	Filename:	example.c
//
#define	Version		"001"
//
//	Edit date:	2021-02-26
//
//	Facility:	Asterisk
//
//	Abstract:	This AGI 'script' is an example of writing an
//			AGI in C.
//
//	Environment:	Asterisk
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	2013-03-17	SLE	Create.
//	001	2021-02-26	SLE	Cleanup.

////////////////////////////////////////|//////////////////////////////////////
// ANSI include files
////////////////////////////////////////|//////////////////////////////////////
#include	<signal.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<syslog.h>

////////////////////////////////////////|//////////////////////////////////////
// Operating system include files
////////////////////////////////////////|//////////////////////////////////////
#include	<syslog.h>

////////////////////////////////////////|//////////////////////////////////////
// Local include files
////////////////////////////////////////|//////////////////////////////////////
#include	"agi.h"
#include	"all.h"
#undef		__GNU_LIBRARY__
#include	"getopt.h"

////////////////////////////////////////|//////////////////////////////////////
// Defines
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Macros
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Typedefs
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////
global	int				debug_mode;
global	int				test_mode;
global	int				verbose_mode;

////////////////////////////////////////|//////////////////////////////////////
// Global functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static functions
////////////////////////////////////////|//////////////////////////////////////
static	void				cleanup
	(
	  void
	);
static	void				hangup
	(
	  void
	);

////////////////////////////////////////|//////////////////////////////////////
// Main function
////////////////////////////////////////|//////////////////////////////////////
global	int				main
	(
	  int				argc
	, char				**argv
	)
	{
	auto	char			city[256];
	static	struct option		long_options[]
		= {
		  {"city", required_argument, 0, 'c'}
		, {"debug-mode", no_argument, &debug_mode, 1}
		, {"help", no_argument, 0, '?'}
		, {"null", no_argument, 0, 0}
		, {"test-mode", no_argument, &test_mode, 1}
		, {"verbose-mode", no_argument, &verbose_mode, 1}
		, {"version", no_argument, 0, 'v'}
		, {0, 0, 0, 0}
		};
	auto	int			optchar;
//	auto	char			response[256];
//	auto	int			a;
//	auto	int			b;
//	auto	int			c;
//	auto	int			idx;
//	auto	int			status;

// set the syslog ident
	setlogmask(LOG_UPTO(LOG_NOTICE));
	openlog(basename(argv[0]), LOG_PID, LOG_USER);

// trap SIGHUP -- caller hung up
	signal(SIGHUP, (void (*)(int))(long int)hangup);

// make sure we clean up after ourselves
	atexit(&cleanup);

// read the AGI environment
	agi_read_environment();

// assume failure
	agi_set_status_failure();

// process the command line arguments
	memset(city, 0, sizeof(city));
	while	(-1 != (optchar = getopt_long(
			  argc
			, argv
			, ""
			, long_options
			, 0
			)
		))
		{
		switch	(optchar)
			{
// city
			case 'c':
				strcpy(city, optarg);
				break;
// help
			case '?':
			default:
				printf("Usage:\t%s\\\n", basename(argv[0]));
				puts("\t\t--debug-mode");
				puts("\t\t--help\\");
				puts("\t\t--null\\");
				puts("\t\t--test-mode\\");
				puts("\t\t--verbose-mode\\");
				puts("\t\t--version");
				exit(EXIT_SUCCESS);
				break;
// version
			case 'v':
				printf("%s version %s\n"
					, *argv
					, Version
					);
				printf("Compiled on %s at %s\n"
					, __DATE__
					, __TIME__
					);
				exit(EXIT_SUCCESS);
				break;

// something automagically handled by getopt_long()
			case 0:
				break;
			}
		}

// set verbose mode
	if	(0 != verbose_mode)
		{
		setlogmask(LOG_UPTO(LOG_INFO));
		}

// set debug mode
	if	(0 != debug_mode)
		{
		setlogmask(LOG_UPTO(LOG_DEBUG));
		}

// say who we are
	syslog(LOG_INFO
		, "Starting, version %s"
		, Version
		);

// this message will be logged if --verbose was specified
	syslog(LOG_INFO, "Before any AGI requests");

// stream a file
	status = agi_stream_file("demo-congrats", "#*0123456789");

// read a variable, say the length
	{
	auto	char			data[8192];
	auto	int			length;
	auto	int			status;
	memset(data, 0, sizeof(data));
	agi_get_variable("TEST", data);
	length = strlen(data);
	status = agi_exec("say number \"%d\" \"%s\""
		, length
		, "*"
		);
	syslog(LOG_DEBUG
		, "The status from 'say number' is %d"
		, status
		);
	}

// say the city if specified
	if	(0 != *city)
		{
		status = agi_stream_file(city, "#*0123456789");
		}

// this message will be logged if -- debug was specified
	syslog(LOG_DEBUG
		, "After any AGI requests"
		);

// set our status
	agi_set_status_success();

// function exit
	return(AGI_SUCCESS);

	}

////////////////////////////////////////|///////////////////////////////|//////
// cleanup()
//
// A good place to clean up temporary files or temporary database rows.
////////////////////////////////////////|///////////////////////////////|//////
static	void				cleanup
	(
	  void
	)
	{
	exit(EXIT_SUCCESS);
	}

////////////////////////////////////////|///////////////////////////////|//////
// hangup()
//
// Asterisk delivers a SIGHUP when the caller hangs up. Since we have
// already registered a process termination function, all we have to
// do is exit.
////////////////////////////////////////|///////////////////////////////|//////
static	void				hangup
	(
	  void
	)
	{
	exit(EXIT_SUCCESS);
	}

// (end of null-agi.c)
