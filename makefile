#
#	Title:		makefile
#
#	Version:	000
#
#	Edit date:	2013-03-17
#
#	Facility:	AGI in C
#
#	Abstract:	This is the makefile for the AGI in C
#			project.
#
#	Environment:	Unix
#
#	Author:		Steven L. Edwards
#
#	Modified by
#
#	000	2013-03-17	SLE	Create.

# define variables
	AGI-BIN				= /var/lib/asterisk/agi-bin/
	CFLAGS				+= -D _GNU_SOURCE
	CFLAGS				+= -Wextra
	CFLAGS				+= -Wformat
	TARBALL				="../agi-in-c.tar.gz-$(shell date +%F)"

all:\
		${START-OF-LIST}\
		example\
		${END-OF-LIST}
	make install

agi.o:\
		${START-OF-LIST}\
		agi.h\
		agi.c\
		all.h\
		${END-OF-LIST}

clean:
	rm --force *~
	rm --force agi.o
	rm --force example
	rm --force example.o

example:\
		${START-OF-LIST}\
		example.o\
		agi.o\
		${END-OF-LIST}
	${LINK.o}\
		$^\
		${OUTPUT_OPTION}\
		${END-OF-LIST}

example.o:\
		${START-OF-LIST}\
		agi.h\
		all.h\
		example.c\
		${END-OF-LIST}
	${COMPILE.c}\
		${OUTPUT_OPTION}\
		example.c

install:\
		${START-OF-LIST}\
		${END-OF-LIST}
	mkdir --parents ${AGI-BIN}/
	-cp --update example ${AGI-BIN}/

tarball:\
		${START-OF-LIST}\
		clean
		${END-OF-LIST}
	tar\
		--create\
		--gzip\
		--file=${TARBALL}\
		*		

# (end of makefile)
