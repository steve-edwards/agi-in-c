
//	Title		all.h
//
//	Version		000
//
//	Edit date:	DD-MMM-YYYY
//
//	Facility:	C programming
//
//	Abstract:	This file defines a bunch of stuff I find useful
//			in all of the programs I write.
//
//	Environment:	C
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	DD-Mmm-YYYY	SLE	Create.

#define	BEGIN_LOCAL_BLOCK	{
#define	END_LOCAL_BLOCK		}
#define	const		/**/
#define	global		/**/

#ifdef	VMS
#define	FATAL(x) \
	if	(0 == (1 & (status = (x)))) \
		{ \
		lib$stop(status); \
		}

#define	FAILURE(x) \
	(0 == (1 & (status = (x))))

#define	SUCCESS(x) \
	(1 & (status = (x)))
#else	// VMS
#define	FATAL(x) \
	if	(EXIT_SUCCESS != (status = (x))) \
		{ \
		exit(EXIT_FAILURE); \
		}

#define	FAILURE(x) \
	(EXIT_SUCCESS != (status = (x)))

#define	RETURN_ON_FAILURE(x) \
	if	(EXIT_FAILURE == (status = (x)))\
		{\
		return(EXIT_FAILURE);\
		}

#define	SUCCESS(x) \
	(EXIT_SUCCESS == (status = (x)))
#define	VERBOSE(x) \
	if	(0 != verbose_mode)\
		{\
		x;\
		}
#endif	// VMS

#define	sizeof_member(type, identifier) \
	(sizeof(((type *)NULL)->identifier))

#ifndef _TYPES_				/* multinet_root:[multinet.include.sys]types.h */
#if	0
typedef	unsigned int	u_int;
typedef	unsigned long	u_long;
typedef	unsigned char	u_char;
typedef	unsigned short	u_short;
#endif
#endif	/* _TYPES_ */

//typedef	enum	{FALSE, TRUE}	T_F;
#define	TRUE	1
#define	FALSE	0
#define	YES	1
#define	NO	0

//
// (end of all.h)
