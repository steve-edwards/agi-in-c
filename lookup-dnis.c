//
//	Filename:	lookup-dnis.c
//
#define	Version		"038"
//
//	Edit date:	2023-06-14
//
//	Facility:	xxxxxx
//
//	Abstract:	This AGI 'script' implements the LOOKUP-DNIS
//			widget.
//
//	Environment:	Asterisk
//
//	Author:		Steven L. Edwards
//
//	Modified by
//
//	000	2009-03-23	SLE	Create.
//	001	2009-10-16	SLE	Log variables if debug.
//	002	2009-10-16	SLE	Add flags.
//	003	2009-11-17	SLE	Add recipe.
//	004	2010-02-11	SLE	Remove extraneous settings of
//					  STATUS channel variable.
//	005	2010-05-30	SLE	Add prompt_recording_mode.
//	006	2010-06-04	SLE	Trim openlog ident using
//					  basename.
//	007	2010-08-23	SLE	Add timezone.
//	008	2010-11-02	SLE	Add clients.active to select.
//	009	2010-11-10	SLE	Use basename() in usage.
//	010	2011-06-27	SLE	Trim leading '+'
//	011	2012-06-20	SLE	Add variables and values.
//	012	2012-06-24	SLE	Make mode variables global.
//	013	2012-09-12	SLE	Change IDX to 3 digits.
//	014	2012-10-18	SLE	Handle active better.
//	015	2012-11-19	SLE	Add LABEL cruft.
//	016	2012-11-20	SLE	Fix bug in setting
//					  non-existent client
//					  variables.
//	017	2014-09-08	SLE	Add value_6.
//					Add variable_6.
//					Add value_7.
//					Add variable_7.
//					Add value_8.
//					Add variable_8.
//					Add value_9.
//					Add variable_9.
//					Add value_10.
//					Add variable_10.
//					Add value_11.
//					Add variable_11.
//					Add value_12.
//					Add variable_12.
//					Add value_13.
//					Add variable_13.
//					Add value_14.
//					Add variable_14.
//					Add value_15.
//					Add variable_15.
//	018	2015-08-10	SLE	Add recording_limit.
//	019	2018-01-02	SLE	Rename steps.timeout to
//					  steps.recording_limit.
//	020	2018-08-05	SLE	Trim whitespace from step
//					  variable values.
//	021	2019-06-19	SLE	Add termination_key.
//	022	2019-07-14	SLE	Rename a couple of select
//					  statements.
//					Add peerip.
//	023	2020-06-05	SLE	Don't syslog() in hangup().
//	024	2020-06-06	SLE	Simplify error message.
//					Add syslog_prefix to all
//					  syslog messages.
//	025	2021-01-15	SLE	Add start_time.
//	026	2021-02-01	SLE	Change call_id to int.
//					Change call_id to global.
//					Change client_id to global.
//	027	2021-02-18	SLE	Add steps.digit_timeout.
//	028	2021-03-23	SLE	Treat *IDX as an integer.
//	029	2021-04-20	SLE	Set MAX-STEP.
//	030	2021-05-13	SLE	Add test_point.
//	031	2022-01-25	SLE	Raise the syslog level for the
//					  duration message.
//	032	2022-07-31	SLE	Add paused.
//	033	2022-09-05	SLE	Add steps.tag.
//	034	2023-04-03	SLE	Add steps.script_text.
//	035	2023-04-25	SLE	Use joins in
//					  select_clients_dnises_recipes.
//					Replace tabs with spaces in
//					  select_clients_dnises_recipes.
//	036	2023-05-03	SLE	Use joins in
//					  select_dnises_recipes_steps.
//					Replace join with inner join
//					  in select_clients_dnises_recipes.
//					Replace join with inner join
//					  in select_dnises_recipes_steps.
//					Replace tabs with spaces in
//					  select_clients.
//					Replace tabs with spaces in
//					  select_dnises.
//	037	2023-05-15	SLE	Add NPC.
//	038	2023-06-14	SLE	Use SYSLOG-PREFIX.

////////////////////////////////////////|//////////////////////////////////////
// ANSI include files
////////////////////////////////////////|//////////////////////////////////////
#include	<ctype.h>
#include	<errno.h>
#include	<signal.h>
#include	<stdarg.h>
#include	<stddef.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<time.h>

////////////////////////////////////////|//////////////////////////////////////
// Operating system include files
////////////////////////////////////////|//////////////////////////////////////
#include	<syslog.h>

////////////////////////////////////////|//////////////////////////////////////
// Local include files
////////////////////////////////////////|//////////////////////////////////////
#include	"agi.h"
#include	"agi-mysql.h"
#include	"all.h"
#include	"getopt.h"
#include	"mysql.h"

////////////////////////////////////////|//////////////////////////////////////
// Defines
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Macros
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Typedefs
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////
global	int				call_id;
global	int				client_id;
global	int				debug_mode;
global	char				syslog_prefix[256];
global	int				test_mode;
global	int				verbose_mode;

////////////////////////////////////////|//////////////////////////////////////
// Global functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Global variables
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External constants
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// External variables
////////////////////////////////////////|//////////////////////////////////////
extern	char				*optarg;

////////////////////////////////////////|//////////////////////////////////////
// External functions
////////////////////////////////////////|//////////////////////////////////////

////////////////////////////////////////|//////////////////////////////////////
// Static constants
////////////////////////////////////////|//////////////////////////////////////
static	const char			no_setup_error_message[]
	= "%s No setup for CLIENT = %d, DNIS = %s, PEERIP = %s"
	;

////////////////////////////////////////|//////////////////////////////////////
// Static variables
////////////////////////////////////////|//////////////////////////////////////
static	MYSQL				mysql;
static	MYSQL_ROW			mysql_row;
static	time_t				start_time;

////////////////////////////////////////|//////////////////////////////////////
// Static functions
////////////////////////////////////////|//////////////////////////////////////
static	void				cleanup
	(
	  void
	);
static	int				lookup_dnis
	(
	  const char			*dnis_pointer
	, const char			*peerip_pointer
	);
static	int				lookup_dnis_by_client_id
	(
	  const char			*client_id_pointer
	, const char			*peerip_pointer
	);
static	void				hangup
	(
	  void
	);

////////////////////////////////////////|//////////////////////////////////////
// Main function
////////////////////////////////////////|//////////////////////////////////////
global	int				main
	(
	  int				argc
	, char				**argv
//	, char				**envp
	)
	{
	auto	char			dnis[256];
	static	struct option		long_options[]
		= {
		  {"client-id", required_argument, 0, 'c'}
		, {"debug-mode", no_argument, &debug_mode, 1}
		, {"help", no_argument, 0, '?'}
		, {"null", no_argument, 0, 0}
		, {"test-mode", no_argument, &test_mode, 1}
		, {"verbose-mode", no_argument, &verbose_mode, 1}
		, {"version", no_argument, 0, 'v'}
		, {0, 0, 0, 0}
		};
	auto	int			optchar;
	auto	char			peerip[256];
	auto	char			*pointer1;
	auto	char			*pointer2;
	auto	int			status;

// save the start time
	start_time = time(0);

// set the syslog ident
	setlogmask(LOG_UPTO(LOG_NOTICE));
	openlog(basename(argv[0]), LOG_PID, LOG_USER);

// trap SIGHUP -- caller hung up
	signal(SIGHUP, (void (*)(int))(long int)hangup);

// read the AGI environment
	agi_read_environment();

// assume failure
	agi_set_status_failure();

// get the CALL-ID
	call_id = agi_get_integer_variable("CALL-ID");

// get the CLIENT-ID
	client_id = agi_get_integer_variable("CLIENT-ID");

// get the DNIS
	memset(dnis, 0, sizeof(dnis));
	agi_get_variable("DNIS", dnis);

// get the PEERIP
	memset(peerip, 0, sizeof(peerip));
	agi_get_variable("PEERIP", peerip);

// get the syslog prefix
	memset(syslog_prefix, 0, sizeof(syslog_prefix));
	agi_get_variable("SYSLOG-PREFIX", syslog_prefix);

// process the command line arguments
	while	(-1 != (optchar
			= getopt_long(
				  argc
				, argv
				, ""
				, long_options
				, 0
				)))
		{
		switch	(optchar)
			{
// client-id
			case 'c':
				lookup_dnis_by_client_id(optarg, peerip);
				break;
// help
			case '?':
			default:
				printf("Usage:\t%s\\\n", basename(argv[0]));
				puts("\t\t--debug-mode");
				puts("\t\t--help\\");
				puts("\t\t--null\\");
				puts("\t\t--verbose-mode\\");
				puts("\t\t--version");
				exit(EXIT_SUCCESS);
				break;
// version
			case 'v':
				printf("%s version %s\n"
					, *argv
					, Version
					);
				printf("Compiled on %s at %s\n"
					, __DATE__
					, __TIME__
					);
				exit(EXIT_SUCCESS);
				break;

// something automagically handled by getopt_long()
			case 0:
				break;
			}
		}

// set verbose mode
	if	(0 != verbose_mode)
		{
		setlogmask(LOG_UPTO(LOG_INFO));
		}

// set debug mode
	if	(0 != debug_mode)
		{
		setlogmask(LOG_UPTO(LOG_DEBUG));
		}

// say who we are
	syslog(LOG_INFO
		, "%s Starting, version %s"
		, syslog_prefix
		, Version
		);

// make sure we clean up after ourselves
	atexit(&cleanup);

// clean the DNIS
	pointer1 = pointer2 = dnis;
	--pointer1;
	while	(0 != *++pointer1)
		{
// keep letters, forcing to lower case
		if	(0 != isalpha(*pointer1))
			{
			*pointer2++ = 32 | *pointer1;
			continue;
			}
// keep numbers
		if	(0 != isdigit(*pointer1))
			{
			*pointer2++ = *pointer1;
			continue;
			}
		}
// terminate the DNIS
	*pointer2 = 0;

// drop leading +
	if	('+' == *dnis)
		{
		auto	char		temp[256];
		strcpy(temp, dnis + 1);
		strcpy(dnis, temp);
		}

// drop leading 1
	if	('1' == *dnis)
		{
		auto	char		temp[256];
		strcpy(temp, dnis + 1);
		strcpy(dnis, temp);
		}

// log the DNIS
//	syslog(LOG_NOTICE
//		, no_setup_error_message
//		, syslog_prefix
//		, client_id
//		, dnis
//		, peerip
//		);

// connect to the database
	if	(EXIT_FAILURE == (status = agi_connect_to_database(
			  &mysql	// context
			)))
		{
		syslog(LOG_ERR
			, "%s %m, mysql_error = \"%s\""
			, syslog_prefix
			, mysql_error(&mysql)
			);
		agi_verbose(
			  "%s Failed to connect to the database"
				", mysql_error = \"%s\""
			, syslog_prefix
			, mysql_error(&mysql)
			);
		exit(EXIT_FAILURE);
		}

// Lookup the DNIS and set the channel variables.
	if	(EXIT_FAILURE == (status = lookup_dnis(dnis, peerip)))
		{
		mysql_close(&mysql);
		return(EXIT_FAILURE);
		}

// log the duration
	syslog(LOG_NOTICE
		, "%s %d seconds"
		, syslog_prefix
		, (int)(time(0) - start_time)
		);

// function exit
	mysql_close(&mysql);
	agi_set_status_success();
	return(EXIT_SUCCESS);

	}

////////////////////////////////////////|///////////////////////////////|//////
// cleanup()
//
////////////////////////////////////////|///////////////////////////////|//////
static	void				cleanup
	(
	  void
	)
	{
	exit(EXIT_SUCCESS);
	}

////////////////////////////////////////|///////////////////////////////|//////
// hangup()
//
// Asterisk delivers a SIGHUP when the caller hangs up. Since we have
// already registered a process termination function, all we have to
// do is exit.
////////////////////////////////////////|///////////////////////////////|//////
static	void				hangup
	(
	  void
	)
	{
//	syslog(LOG_ERR, "hangup detected");
	exit(EXIT_SUCCESS);
	}

////////////////////////////////////////|///////////////////////////////|//////
// lookup_dnis()
//
// Lookup the DNIS and set the channel variables.
////////////////////////////////////////|///////////////////////////////|//////
static	int				lookup_dnis
	(
	  const char			*dnis_pointer
	, const char			*peerip_pointer
	)
	{
//	auto	int			client_id;
	auto	int			max_idx;
	static	const char		select_clients[]
		= "select"
				"  'recording_limit'"
				", recording_limit"
				", variable_1"
				", value_1"
				", variable_2"
				", value_2"
				", variable_3"
				", value_3"
				", variable_4"
				", value_4"
				", variable_5"
				", value_5"
				", variable_6"
				", value_6"
				", variable_7"
				", value_7"
				", variable_8"
				", value_8"
				", variable_9"
				", value_9"
				", variable_10"
				", value_10"
				", variable_11"
				", value_11"
				", variable_12"
				", value_12"
				", variable_13"
				", value_13"
				", variable_14"
				", value_14"
				", variable_15"
				", value_15"
			" from"
				"  clients"
			" where '%d' = client_id"
			;
	auto	int			column;
	auto	MYSQL_FIELD		*fields;
	auto	int			index;
	auto	int			num_fields;
	auto	int			num_rows;
	auto	MYSQL_RES		*result_pointer;
	static	const char		select_clients_dnises_recipes[]
		= "select"
				"  clients.client_id"
					// client ID must be first
				", recipes.recipe as recipe"
					// recipe must be second
				", confirm_prompt"
				", goodbye"
				", initial_language"
				", outbound_dialstring"
				", paused"
				", prompt_recording_mode"
				", termination_key"
				", test_point"
				", timezone"
				", verification_prompt_a"
				", verification_prompt_b"
				", you_entered_prompt"
			" from  dnises"
			" inner join  clients"
				" on clients.client_id = dnises.client_id"
			" inner join  recipes"
				" on dnises.recipe = recipes.recipe"
			" where clients.active = 'YES'"
			" and   dnis = %s"
				;
	auto	int			status;
	static	const char		select_dnises_recipes_steps[]
		= "select"
				"  steps.idx"
					// idx must be the first column
				", steps.active"
				", steps.data"
					// data must precede type
				", steps.digit_timeout"
				", steps.flags + 0 as flags"
				", steps.max_length"
				", steps.min_length"
				", steps.prompt"
				", steps.recording_limit"
				", steps.retry_prompt"
				", steps.script_text"
				", steps.tag"
				", steps.type"
					// type must follow data
			" from"
				"  dnises"
				"  inner join recipes"
					" on    dnises.recipe = recipes.recipe"
				"  inner join steps"
					" on    dnises.recipe = steps.recipe"
					" and   steps.active in ('NPC', 'YES')"
			" where '%s' = dnis"
			" and   steps.active in ('NPC', 'YES')"
			" order by idx"
			;

// look for the recipe parameters
	exec_sql(&mysql
		, &result_pointer
		, select_clients_dnises_recipes
					// select statement template
		, dnis_pointer		// dnis
		);

// complain if not found
	if	((1 != mysql_num_rows(result_pointer))
	||	 ((0 == (mysql_row = mysql_fetch_row(result_pointer)))))
		{
		syslog(LOG_ERR
			, no_setup_error_message
			, syslog_prefix
			, client_id
			, dnis_pointer
			, peerip_pointer
			);
		return(EXIT_FAILURE);
		}

// save the client id
	client_id = atoi(mysql_row[0]);

// update the syslog prefix
	sprintf(syslog_prefix
		, "%03d-%08d"
		, client_id
		, call_id
		);

// set the recipe variables
	num_fields = mysql_num_fields(result_pointer);
	fields = mysql_fetch_fields(result_pointer);
	for	(column = 0; column < num_fields; ++column)
		{
		auto	char		*pointer;
		auto	char		temp[1024];
		if	((0 == mysql_row[column])
		||	 ('(' == *mysql_row[column]))
			{
			continue;
			}
		strcpy(temp, fields[column].name);
		pointer = temp;
		while	(0 != *pointer)
			{
			if	(islower(*pointer))
				{
				*pointer = toupper(*pointer);
				}
			if	('_' == *pointer)
				{
				*pointer = '-';
				}
			++pointer;
			}
		syslog(LOG_DEBUG
			, "%s %s = \"%s\""
			, syslog_prefix
			, temp
			, mysql_row[column]
			);
		agi_set_variable(temp, mysql_row[column]);
		}

// look for the step parameters
	status = exec_sql(&mysql
		, &result_pointer
		, select_dnises_recipes_steps
					// select statement template
		, dnis_pointer		// dnis
		);
	if	(0 != status)
		{
		syslog(LOG_ERR
			, "%s %m, mysql_error = \"%s\""
			, syslog_prefix
			, mysql_error(&mysql)
			);
		}

// complain if not found
	if	(1 > (num_rows = mysql_num_rows(result_pointer)))
		{
		syslog(LOG_ERR
			, no_setup_error_message
			, syslog_prefix
			, client_id
			, dnis_pointer
			, peerip_pointer
			);
		return(EXIT_FAILURE);
		}

// walk through the rows
	max_idx = 0;
	for	(index = 0; ; ++index)
		{
		mysql_data_seek(result_pointer, index);
		if	(0 == (mysql_row = mysql_fetch_row(result_pointer)))
			{
			break;
			}

// save the max idx
 		max_idx = atoi(mysql_row[0]);

// set the step variables
		num_fields = mysql_num_fields(result_pointer);
		fields = mysql_fetch_fields(result_pointer);
		for	(column = 0; column < num_fields; ++column)
			{
			auto	char		data[1024];
			auto	char		*pointer;
			auto	char		*pointer2;
			auto	char		name[256];
			auto	char		value[1024];
			if	((0 == mysql_row[column])
			||	 ('(' == *mysql_row[column]))
				{
				continue;
				}
			sprintf(name
				, "STEP-%d-%s"
				, atoi(mysql_row[0])
				, fields[column].name
				);
			pointer = name + 5;
			while	(0 != *pointer)
				{
				if	(islower(*pointer))
					{
					*pointer = toupper(*pointer);
					}
				if	('_' == *pointer)
					{
					*pointer = '-';
					}
				++pointer;
				}
			*pointer = 0;
			pointer2 = value;
			pointer = mysql_row[column];
			while	(0 != *pointer)
				{
//				if	(isspace(*pointer))
//					{
//					++pointer;
//					continue;
//					}
				*pointer2++ = *pointer++;
				}
			*pointer2 = 0;
			syslog(LOG_DEBUG
				, "%s %s = \"%s\""
				, syslog_prefix
				, name
				, value
				);
			agi_set_variable(name, value);
// LABELs take a little bit more work
//printf("mysql_row[0] = %s, mysql_row[column] = %s, fields[column].name = %s\n"
//, mysql_row[0]
//, mysql_row[column]
//, fields[column].name
//);
			if	(0 == strcmp("data", fields[column].name))
				{
				strcpy(data, mysql_row[column]);
				}
			if	((0 == strcmp("type", fields[column].name))
			&&	 (0 == strcmp("LABEL", mysql_row[column])))
				{
				auto	char	label[256];
				memset(label, 0, sizeof(label));
				sprintf(label, "LABEL-%s", data);
				syslog(LOG_DEBUG
					, "%s %s = \"%s\""
					, syslog_prefix
					, label
					, data
					);
				agi_set_variable(label, mysql_row[0]);
				}
			}
		}

// set the MAX-STEP channel variable
	agi_set_integer_variable("MAX-STEP", max_idx);

// look for the client variables (really should be recipe variables
// but adding these to the clients table kept the client maintenance
// web page simple.
	exec_sql(&mysql
		, &result_pointer
		, select_clients	// select statement template
		, client_id		// client_id
		);

// complain if not found
	if	((1 != mysql_num_rows(result_pointer))
	||	 ((0 == (mysql_row = mysql_fetch_row(result_pointer)))))
		{
		syslog(LOG_ERR
			, no_setup_error_message
			, syslog_prefix
			, client_id
			, dnis_pointer
			, peerip_pointer
			);
		return(EXIT_FAILURE);
		}

// set the client variables
	num_fields = mysql_num_fields(result_pointer);
	fields = mysql_fetch_fields(result_pointer);
	for	(column = 0; column < num_fields; ++column)
		{
		auto	char		*pointer;
		auto	char		temp[1024];
		if	((0 == mysql_row[column])
		||	 (0 == *mysql_row[column])
		||	 ('(' == *mysql_row[column]))
			{
			continue;
			}
		strcpy(temp, mysql_row[column]);
		pointer = temp;
		while	(0 != *pointer)
			{
			if	(islower(*pointer))
				{
				*pointer = toupper(*pointer);
				}
			if	('_' == *pointer)
				{
				*pointer = '-';
				}
			++pointer;
			}
		++column;
		if	((0 == mysql_row[column])
		||	 ('(' == *mysql_row[column]))
			{
			continue;
			}
		syslog(LOG_DEBUG
			, "%s %s = \"%s\""
			, syslog_prefix
			, temp
			, mysql_row[column]
			);
		agi_set_variable(temp, mysql_row[column]);
		}

// free the result set
	mysql_free_result(result_pointer);

// function exit
	return(EXIT_SUCCESS);

	}

////////////////////////////////////////|///////////////////////////////|//////
// lookup_dnis_by_client_id()
//
// Lookup the DNIS by the client ID
////////////////////////////////////////|///////////////////////////////|//////
static	int				lookup_dnis_by_client_id
	(
	  const char			*client_id_pointer
	, const char			*peerip_pointer
	)
	{
//	auto	int			client_id;
	static	const char		select_dnises[]
		= "select"
				"  dnis"
			" from"
				"  dnises"
			" where '%s' = client_id"
			" limit 1"
			;
//	auto	int			column;
//	auto	MYSQL_FIELD		*fields;
//	auto	int			index;
//	auto	int			num_fields;
//	auto	int			num_rows;
	auto	MYSQL_RES		*result_pointer;
//	auto	int			status;

// look for the dnis
	exec_sql(&mysql
		, &result_pointer
		, select_dnises		// select statement template
		, client_id_pointer	// client_id
		);

// complain if not found
	if	((1 != mysql_num_rows(result_pointer))
	||	 ((0 == (mysql_row = mysql_fetch_row(result_pointer)))))
		{
		syslog(LOG_ERR
			, no_setup_error_message
			, syslog_prefix
			, client_id_pointer
			, ""
			, peerip_pointer
			);
		return(EXIT_FAILURE);
		}

// save the client id
	agi_set_variable("DNIS", mysql_row[0]);

// free the result set
	mysql_free_result(result_pointer);

// function exit
	return(EXIT_SUCCESS);

	}

// (end of lookup-dnis.c)
